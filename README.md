# **netsim arg assistant**

## **about**

Slightly modified version of `netsim` program to facilitate easier creation/updating of network conditions by prompting the user to provide argument values if the program is called without any parameters. All original `netsim` function remains. If any parameter is provided, no additional user input will occur (just as before).

## **changes**

If no arguments passed by user, ie: 

    netsim

then user is prompted whether want to provide additional network parameters ('no' is default behavior):

    configure network parameters? Y/[n]: 

if `Yes/yes/Y/y` is returned (any word beginning with a 'Y'), then each parameter will be iterated over, wereby user can specify a value or, if none provided, the default value is passed in:
```
bandwidth Mbps,  .001-10: [1]
latency ms,  10-5000: [10]
delay %,  0-100: [0]
drop %,  0-100: [0]
reorder %,  0-100: [0]
duplicate %,  0-100: [0]
corrupt %,  0-100: [0]
queue limit Mb,  1-1000: [1000]
```

the values are then passed to the program as if they had been passed in on the command line. 

## **notes**

an additional module is required to be specified at the top: 

    import sys
<br>

## **hopefully this helps! incorporate into default if deemed useful**
